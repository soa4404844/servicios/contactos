package com.soa.contactos.domain.service;

import com.soa.contactos.domain.model.Contacto;
import com.soa.contactos.domain.repository.DomainContactoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DomainContactoServiceImpl implements DomainContactoService {

    @Autowired
    private DomainContactoRepository domainContactoRepository;

    @Override
    public Contacto saveContacto(Contacto contacto) {
        System.out.println(contacto);
        return domainContactoRepository.save(contacto);
    }

    @Override
    public Contacto getContacto(Contacto contacto) {
        return domainContactoRepository.findById(contacto.getContactoID())
                .orElse(Contacto.builder().build());
    }
}
