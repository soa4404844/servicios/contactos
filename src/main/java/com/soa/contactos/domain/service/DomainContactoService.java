package com.soa.contactos.domain.service;

import com.soa.contactos.domain.model.Contacto;
import org.springframework.stereotype.Repository;

@Repository
public interface DomainContactoService {

    Contacto saveContacto(Contacto contacto);

    Contacto getContacto(Contacto contacto);

}
