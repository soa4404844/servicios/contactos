package com.soa.contactos.domain.repository;

import com.soa.contactos.domain.model.Contacto;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DomainContactoRepository extends CrudRepository<Contacto, Integer> {

    @Query("""
            SELECT C FROM CONTACTO C
            """)
    List<Contacto> findByPorParametro(
            @Param("parametro") String parametro,
            @Param("contactoTabla") Contacto contacto
    );

}
