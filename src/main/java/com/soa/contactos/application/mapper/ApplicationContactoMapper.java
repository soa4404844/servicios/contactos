package com.soa.contactos.application.mapper;

import com.soa.contactos.domain.model.Contacto;
import com.soa.contactos.application.model.GuardarContacto;
import com.soa.contactos.application.model.ObtenerContacto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ApplicationContactoMapper {

    Contacto guardarContactoToContacto(GuardarContacto guardarContacto);

    GuardarContacto contactoToGuardarContacto(Contacto contacto);

    Contacto obtenerContactoToContacto(ObtenerContacto obtenerContacto);

    ObtenerContacto contactoToObtenerContacto(Contacto contacto);


}
