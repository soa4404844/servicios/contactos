package com.soa.contactos.application.service;


import com.soa.contactos.application.mapper.ApplicationContactoMapper;
import com.soa.contactos.domain.model.Contacto;
import com.soa.contactos.domain.service.DomainContactoService;
import com.soa.contactos.application.api.ContactoApiDelegate;
import com.soa.contactos.application.model.GuardarContacto;
import com.soa.contactos.application.model.ObtenerContacto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


@Component
public class ApplicationContactoServiceImpl implements ContactoApiDelegate {

    @Autowired
    private ApplicationContactoMapper applicationContactoMapper;

    @Autowired
    private DomainContactoService domainContactoService;


    @Override
    public ResponseEntity<GuardarContacto> guardarContacto(GuardarContacto guardarContacto) {
        var contacto = domainContactoService.saveContacto(
                applicationContactoMapper.guardarContactoToContacto(guardarContacto)
        );

        return ResponseEntity.ok(
                applicationContactoMapper.contactoToGuardarContacto(contacto)
        );
    }

    @Override
    public ResponseEntity<ObtenerContacto> obtenercontacto(Integer contactoId) {
        var contacto = domainContactoService.getContacto(
                Contacto.builder().contactoID(contactoId).build()
        );

        return ResponseEntity.ok(
                applicationContactoMapper.contactoToObtenerContacto(contacto)
        );
    }

}

